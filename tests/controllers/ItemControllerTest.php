<?php
namespace TestApi\Tests\Controllers;

use PHPUnit\Framework\TestCase;
use TestApi\Models\Request;
use TestApi\Models\Config;
use TestApi\Controllers\ListController;

/**
 * Test /list/* requests
 *
 * @author vk
 */
class ItemControllerTest extends TestCase
{
    /**
     * @var string
     */
    const CONTROLLER_NAME = 'item';
    
    /**
     * Id test item
     * @var int
     */
    protected static $testId = 0;
    
    /**
     * @var \PDO
     */
    protected $db = null;
    
    /**
     * @var Request
     */
    protected $request = null;
    
    /**
     * @var \TestApi\Controllers\Controller
     */
    protected $controller = null;
    
    public function setup()
    {
        $this->request = Request::getInstance();
        $config = Config::getInstance('config.ini');
        $db_config = $config->getDB();
        $this->db = new \PDO(
            "pgsql:host={$db_config->host};"
            . "port={$db_config->port};"
            . "dbname={$db_config->name};"
            . "user={$db_config->user};"
            . "password={$db_config->password}"
        );
        
        $controllerClass = "\\TestApi\\Controllers\\"
            . ucfirst(static::CONTROLLER_NAME) . 'Controller';
        
        $this->controller = new $controllerClass($this->db, $this->request);
    }
    
    public function testAddRow()
    {
        $this->request->setParams(
            array(
                'name'        => 'test',
                'description' => 'test description',
                'status'      => '1'
            )
        );
        $item_id = (int)$this->controller->postItem();
        $this->assertTrue(!empty($item_id));
        static::$testId = $item_id;
    }

    public function testGetRow()
    {
        $this->request->setId(static::$testId);
        $item = $this->controller->getItem();
        $this->assertTrue(is_object($item));
        $this->assertObjectHasAttribute('name', $item);
        $this->assertEquals($item->status, 1);
    }
    
    public function testUpdateRow()
    {
        $this->request->setId(static::$testId);
        $this->request->setParams(
            array(
                'description'    => 'test desc'
            )
        );
        $this->assertTrue($this->controller->putItem());
        $item = $this->controller->getItem();
        $this->assertEquals($item->description, 'test desc');
    }
    
    public function testDeleteRow()
    {
        $this->request->setId(static::$testId);
        $this->assertTrue($this->controller->deleteItem());
    }
}
