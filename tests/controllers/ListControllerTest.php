<?php
namespace TestApi\Tests\Controllers;

use PHPUnit\Framework\TestCase;
use TestApi\Models\Request;
use TestApi\Models\Config;
use TestApi\Controllers\ItemController;
use TestApi\Controllers\ListController;

/**
 * Test /List/* requests
 *
 * @author vladimir
 */
class ListControllerTest extends TestCase
{
    /**
     * @var string
     */
    const CONTROLLER_NAME = 'List';
    
    /**
     * Id test book
     * @var int
     */
    protected static $testId = 0;
    
    /**
     * @var \PDO
     */
    protected $db = null;
    
    /**
     * @var Request
     */
    protected $request = null;
    
    /**
     * @var \TestApi\Controllers\Controller
     */
    protected $controller = null;
    
    public function setup()
    {
        $this->request = Request::getInstance();
        $config = Config::getInstance('config.ini');
        $db_config = $config->getDB();
        $this->db = new \PDO(
            "pgsql:host={$db_config->host};"
            . "port={$db_config->port};"
            . "dbname={$db_config->name};"
            . "user={$db_config->user};"
            . "password={$db_config->password}"
        );
        
        $controllerClass = "\\TestApi\\Controllers\\"
            . ucfirst(static::CONTROLLER_NAME) . 'Controller';
        
        $this->controller = new $controllerClass($this->db, $this->request);
    }
    
    public function testAddRow()
    {
        $this->request->setParams(
            array(
                'name'        => 'test',
                'description' => 'test',
                'authors'     => 'test author',
                'publishing_year' => '2017'
            )
        );
        
        $bookController = new ItemController($this->db, $this->request);
        $book_id = (int)$bookController->postItem();
        $this->assertTrue(!empty($book_id));
        static::$testId = $book_id;
    }
    
    public function testGetList()
    {
        # author & year
        $this->request->setParams(
            array(
                'author' => 'test author',
                'year'   => '2017'
            )
        );
        $this->assertNotEmpty($this->controller->getList());
        
        # order
        $this->request->setParam('order', array('publishing_year' => 'ASC'));
        $this->assertNotEmpty($this->controller->getList());
        
        # pagination
        $this->request->setParams(array('limit' => 1));
        $this->assertEquals(count($this->controller->getList()), 1);
        
        $this->request->setParams(null);
    }
}
