<?php
namespace TestApi\Tests\Models;

use PHPUnit\Framework\TestCase;
use TestApi\Models\Request;

/**
 * @author vk
 */
class RequestTest extends TestCase
{
    /**
     * @var Request
     */
    private $request = null;
    
    /**
     * @var string
     */
    private $uri = '/controller/method/id';

    public function setup()
    {
        $this->request = Request::getInstance($this->uri);
        $this->assertNotEmpty($this->request);
    }
    
    public function testRoute()
    {
        $this->assertNotEmpty($this->request->getUri());
        $this->assertEquals($this->request->getUri(), $this->uri);
        $this->assertEquals($this->request->getController(), 'controller');
        $this->assertEquals($this->request->getPath(), array('method', 'id'));
        $this->assertEquals($this->request->getMethod(), 'get');
        $this->assertEquals($this->request->getId(), 'id');
    }
    
    public function testParams()
    {
        $this->request->setParam('param', 'value');
        $this->assertNotEmpty($this->request->getParams());
        $this->assertTrue($this->request->issetParam('param'));
        $this->assertEquals($this->request->getParam('param'), 'value');
    }
}
