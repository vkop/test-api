<?php
namespace TestApi\Tests\Models;

use TestApi\Models\ItemsQueryBuilder;

/**
 * @author vk
 */
class ItemsQueryBuilderTest extends TestCase
{
    /**
     * @var \TestApi\Models\QueryBuilderTest 
     */
    protected $qb = null;
    
    /**
     * @var string
     */
    protected $table = 'items n';
    
    public function setup()
    {
        $this->qb = new ItemsQueryBuilder();
        $this->qb->from($this->table);
    }
    
    public function testInsert()
    {
        $quary = $this->qb->insert(array('string', 42, true))->query();
        $this->assertEquals($quary, "INSERT INTO {$this->table} VALUES ('string', 42, 1);");
    }
    
    public function testSelect()
    {
        $quary = $this->qb->select(array('column'))
                ->where('test=1')
                ->query();
        $this->assertEquals($quary, "SELECT column FROM {$this->table} WHERE test=1;");
        
        $this->assertEquals(
            $this->qb->order(array('column' => 'ASC'))->query(),
            "SELECT column FROM {$this->table} WHERE test=1 ORDER BY column ASC;"
        );
        
        $this->assertEquals(
            $this->qb->group('column')->query(),
            "SELECT column FROM {$this->table} WHERE test=1 ORDER BY column ASC"
            . " GROUP BY column;"
        );
        
        $this->assertEquals(
            $this->qb->limit(1)->query(),
            "SELECT column FROM {$this->table} WHERE test=1 "
            . "ORDER BY column ASC GROUP BY column LIMIT 1;"
        );
    }
    
    public function testUpdate()
    {
        $quary = $this->qb->update(
            array(
                'string' => 'value',
                'int' => 42,
                'bool' => true
            )
        )->query();
        $this->assertEquals($quary, "UPDATE {$this->table} SET string = 'value', int = 42, bool = 1;");
    }
    
    public function testDelete()
    {
        $quary = $this->qb->delete()->where('test=true')->query();
        $this->assertEquals($quary, "DELETE FROM {$this->table} WHERE test=true;");
    }
    
    public function testSetStatus()
    {
        $quary = $this->qb->select()->setPublishYear(1)->query();
        $this->assertEquals($quary, "SELECT * FROM {$this->table} WHERE n.status=1;");
    }
    
    public function testSetName()
    {
        $quary = $this->qb->select()->setName('test')->query();
        $this->assertEquals($quary, "SELECT * FROM {$this->table} WHERE n.name LIKE '%test%';");
    }
}
