<?php
namespace TestApi\Controllers;

use TestApi\Models\ItemsQueryBuilder;

/**
 * Controller for working with items
 * @author vk
 */
class ItemController extends Controller
{
    /**
     * Return item by id
     * @return mixed
     */
    public function getItem()
    {
        if (!$id = $this->request->getId()) {
            $this->errors[] = "Not set item's id.";
            return;
        }
        
        $query = new ItemsQueryBuilder();
        $query->select()->where("id = $id");
        
        if (!$result = $this->pdo->query($query->query())) {
            return null;
        }
        
        if (!$book = $result->fetchObject()) {
            $this->errors[] = "Not found item with id $id.";
            $this->code_error = 403;
            return null;
        }
        
        return $book;
    }
    
    /**
     * Save item
     * @return int|boolean
     */
    public function postItem()
    {
        $request = $this->request->getParams(true);
        
        if (empty($request)) {
            $this->errors[] = "Empty request.";
            return false;
        }
        
        # Required params for insert query
        $required_params = array('name');
        
        if (!$this->checkRequiredParams($required_params)) {
            return false;
        }
        
        $columns = array_keys($request);
        $sql = "INSERT INTO %table% (". implode(', ', $columns).") "
            . "VALUES (:".implode(', :', $columns).");";
        $query = new ItemsQueryBuilder($sql);
        $stm = $this->pdo->prepare($query->query());
        
        if (!$stm->execute($request)) {
            $error_info = $stm->errorInfo();
            $this->errors[] = "DB " . $error_info[2];
            return false;
        }
        
        return $this->pdo->lastInsertId();
    }
    
    /**
     * Update item
     * @return boolean
     */
    public function putItem()
    {
        if (!$id = (int)$this->request->getId()) {
            $this->errors[] = "Not set item's id.";
            return false;
        }
        
        $request = $this->request->getParams(true);
        
        if (empty($request)) {
            $this->errors[] = "Empty request.";
            return false;
        }
        
        $update_sql = '';
        
        foreach ($request as $column => $field) {
            $update_sql .= "$column=:$column, ";
        }

        $query = new ItemsQueryBuilder(
            "UPDATE %table% SET ".substr($update_sql, 0, -2)." WHERE id = :id"
        );
        $stm = $this->pdo->prepare($query->query());
        $request["id"] = $id;
        
        if (!$stm->execute($request)) {
            $error_info = $stm->errorInfo();
            $this->errors[] = "DB " . $error_info[2];
            return false;
        }
        
        return true;
    }
    
    /**
     * Delete book
     * @return boolean
     */
    public function deleteItem()
    {
        if (!$id = (int)$this->request->getId()) {
            $this->errors[] = "Not set item's id.";
            return false;
        }
        
        $query = new ItemsQueryBuilder("DELETE FROM %table% WHERE id = :id");
        $stm = $this->pdo->prepare($query->query());
        $stm->bindParam(':id', $id);
        return $stm->execute();
    }
}
