<?php
namespace TestApi\Controllers;

use PDO;
use TestApi\Models\Request;

/**
 * Controller API
 * 
 * @author vk
 */
abstract class Controller
{
    /**
     * PHP Data Objects
     * @var PDO
     */
    protected $pdo;
    
    /**
     * Request object
     * @var Request
     */
    protected $request;
    
    /**
     * Description of errors
     * @var array
     */
    protected $errors = array();
    
    /**
     * Code of error
     * @var int
     */
    protected $code_error = 400;
    
    /**
     * @param PDO $pdo
     * @param Request $request
     */
    public function __construct(PDO $pdo, Request $request)
    {
        $this->pdo = $pdo;
        $this->request = $request;
    }
    
    /**
     * Return responce in json-format
     * @return mixed
     */
    public function response()
    {
        if (!$this->pdo) {
            $this->errors = 'Error: Database not connect';
            $response = false;
        } elseif (empty($this->request->getController())) {
            $response = $this->getIndex();
        } elseif ($method = $this->method()) {
            $response = $this->$method();
        } else {
            $response = false;
        }
        
        if (!$response) {
            $error_msg = !empty($this->errors) ? implode(' ', $this->errors) : 'Unknown error';
            header("Error: $error_msg", false, $this->code_error);
        }
        
        return json_encode($response);
    }
    
    /**
     * Get executive method's name for request if here is
     * @return string|false
     */
    private function method()
    {
        $request_method = $this->request->getMethod();
        $method = $request_method . ucfirst($this->request->getController());
        $path = $this->request->getPath();
        
        if (!empty($path)) {
            $method_path = array();
            
            foreach ($path as $item) {
                $method_path[] = ucfirst(strtolower($item));
            }
            
            $method = $request_method . implode('', $method_path);
            
            # if not exist method, check for method with id
            if (!method_exists($this, $method)) {
                $id = array_pop($path);
                array_pop($method_path);
                $method = $request_method . implode('', $method_path);
                
                if (method_exists($this, $method)) {
                    $this->request->setId($id);
                } else {
                    $method = $request_method . ucfirst($this->request->getController());
                }
            }
        }
        
        return method_exists($this, $method) ? $method : false;
    }
    
    /**
     * Check for occupancy required 'get' params
     * @param type $params
     * @return bool
     */
    protected function checkRequiredParams($params)
    {
        $this->errors = array();
        
        foreach ($params as $param) {
            if (!$this->request->issetParam($param)) {
                $this->errors[] = "Not set $param param.";
            }
        }
        
        return empty($this->errors);
    }
}
