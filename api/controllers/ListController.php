<?php
namespace TestApi\Controllers;

use TestApi\Models\ItemsQueryBuilder;

/**
 * Controller for working with list items
 * @author vk
 */
class ListController extends Controller
{
    /**
     * Return List items
     * @return array
     */
    public function getList()
    {
        $query = new ItemsQueryBuilder();
        $query->select('*', (bool)$this->request->getParam('distinct'));
        
        # search by name
        if ($name = $this->request->getParam('name')) {
            $query->setName($name);
        }
        
        # search by status
        if ($status = (int) $this->request->getParam('status')) {
            $query->setStatus($status);
        }
        
        # order
        if ($order = $this->request->getParam('order')) {
            $query->order($order);
        }
        
        # pagination
        if ($limit = (int) $this->request->getParam('limit')) {
            $query->limit($limit, $this->request->getParam('start'));
        }
        
        $result = $this->pdo->query($query->query());
        return $result ? $result->fetchAll() : array();
    }
}
