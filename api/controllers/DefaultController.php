<?php
namespace TestApi\Controllers;

use TestApi\Models\ItemsQueryBuilder;

/**
 * Default controller
 * @author vk
 */
class DefaultController extends Controller
{
    /**
     * Return the reference about API
     */
    public function getDefault()
    {
        return 'Test Api 0.1';
    }
}
