<?php
namespace TestApi\Models;

/**
 * Class for work with request
 * 
 * @author vk
 */
class Request
{
    protected static $instance;
    
    /**
     * Uri
     * @var string
     */
    protected static $uri = '';

    /**
     * Request method
     * @var string
     */
    protected static $method = '';
    
    /**
     * Controller
     * @var string
     */
    protected static $controller = 'index';

    /**
     * Subject
     * @var array|null
     */
    protected static $path = null;
    
    /**
     * Id
     * @var string|null
     */
    protected static $id = null;

    /**
     * Params
     * @var stdClass
     */
    protected static $params = null;
    
    private function __construct()
    {

    }

    /**
     * Singlton method
     * @param string $uri
     * @return Request
     */
    public static function getInstance($uri = '')
    {
        if (self::$instance === null) {
            self::$instance = new self;
            $uri = !empty($uri) ? $uri : filter_input(INPUT_SERVER, "REQUEST_URI");
            
            if (strpos($uri, '?')) {
                $uri = substr($uri, 0, strpos($uri, '?'));
            }

            self::$uri = $uri;
            $uri_ar = explode('/', trim($uri, '/'));
            self::$method = strtolower(filter_input(INPUT_SERVER, "REQUEST_METHOD"));
            
            if (empty(self::$method)) {
                self::$method = 'get';
            }
            
            self::$controller = strtolower(array_shift($uri_ar));
            self::$path = $uri_ar;
            self::$id = end($uri_ar);
            self::$params = (object)filter_input_array(INPUT_GET);
        }
 
        return self::$instance;
    }
 
    private function __clone()
    {
        
    }

    private function __wakeup()
    {
        
    }
    
    /**
     * Return uri current request
     * @return string
     */
    public function getUri()
    {
        return self::$uri;
    }
    
    /**
     * Return method request (GET, POST, PUT, DELETE etc)
     * @return string
     */
    public function getMethod()
    {
        return self::$method;
    }
    
    /**
     * Return controller for current request
     * @return string
     */
    public function getController()
    {
        return self::$controller;
    }
    
    /**
     * Return path for current request
     * @return array|null
     */
    public function getPath()
    {
        return self::$path;
    }
    
    /**
     * Return subject for current request
     * @return string|null
     */
    public function getId()
    {
        return self::$id;
    }
    
    /**
     * @param mixed|null $id
     */
    public function setId($id)
    {
        if (is_string($id)) {
            self::$id = urldecode($id);
        } else {
            self::$id = $id;
        }
    }
    
    /**
     * @param bool $in_array
     * @return mixed
     */
    public function getParams($in_array = false)
    {
        return $in_array ? (array)self::$params : self::$params;
    }
    
    /**
     * @param string $param
     * @return bool
     */
    public function issetParam($param)
    {
        return property_exists(self::$params, $param);
    }
    
    /**
     * @param string $param
     * @return mixed
     */
    public function getParam($param)
    {
        return $this->issetParam($param) ? self::$params->{$param} : false;
    }
    
    /**
     * @param string $param
     */
    public function setParam($param, $value)
    {
        self::$params->{$param} = $value;
    }
    
    /**
     * @param array|stdClass $params
     */
    public function setParams($params)
    {
        self::$params = (object)$params;
    }
}
