<?php
namespace TestApi\Models;

/**
 * Class for work with config
 * 
 * @author vk
 */
class Config
{
    protected static $instance;
    
    /**
     * Access to the database
     * @var stdClass
     */
    protected static $db;
    
    /**
     * Admin config
     * @var stdClass
     */
    protected static $admin;

    private function __construct()
    {

    }

    /**
     * Singlton
     * @param string $file
     * @return Request
     */
    public static function getInstance($file = '')
    {
        if (self::$instance === null) {
            self::$instance = new self;
            $config = parse_ini_file($file);
            
            self::$db = (object) array(
                'driver'   => $config['db_driver'],
                'host'     => $config['db_host'],
                'port'     => $config['db_port'],
                'name'     => $config['db_name'],
                'user'     => $config['db_user'],
                'password' => $config['db_password']
            );
            
            self::$admin = (object) array(
                'user'     => $config['admin_user'],
                'password' => $config['admin_password']
            );
        }
 
        return self::$instance;
    }
 
    private function __clone()
    {
        
    }

    private function __wakeup()
    {
        
    }
    
    /**
     * Return config for connect to database
     * @return string
     */
    public function getDB()
    {
        return self::$db;
    }
    
    /**
     * Return config for admin
     * @return string
     */
    public function getAdmin()
    {
        return self::$admin;
    }
}
