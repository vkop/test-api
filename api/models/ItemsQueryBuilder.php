<?php
namespace TestApi\Models;

/**
 * Constructor query for table "items"
 *
 * @author vk
 */
class ItemsQueryBuilder extends QueryBuilder
{
    /**
     * Name table in the database
     * @var string
     */
    protected $table = 'items n';

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name)
    {
        $this->where("n.authors LIKE '%$name%'");
        return $this;
    }
    
    /**
     * @param int $status
     * @return $this
     */
    public function setStatus(int $status)
    {
        $this->where("n.publishing_year=$status");
        return $this;
    }
}
