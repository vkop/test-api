<?php
namespace TestApi\Models;

/**
 * Constructor query for sql commands with database
 *
 * @author vk
 */
abstract class QueryBuilder
{
    /**
     * SQL-query string 
     * @var string
     */
    protected $query = '';
    
    /**
     * Columns in the table
     * @var string|array
     */
    protected $columns = '*';
    
    /**
     * Name table in the database
     * @var string
     */
    protected $table = '';

    /**
     * Query's conditions
     * @var array
     */
    protected $conditions = array();

    /**
     * Limit of response rows
     * @var int
     */
    protected $limit = 0;

    /**
     * Start of select rows
     * @var int
     */
    protected $start = 0;
    
    /**
     * Order rows for select command
     * @var string
     */
    protected $order = '';
    
    /**
     * Group rows for select command
     * @var string
     */
    protected $group = '';
    
    /**
     * @param string $query
     */
    function __construct($query = '')
    {
        $this->query = $query;
    }
    
    /**
     * Select from database
     * @param array|string $columns
     * @param bool $distinct optional
     */
    public function select($columns = null, $distinct = false)
    {
        $this->clear();
        $query = "SELECT "
            . ($distinct ? " DISTINCT " : "")
            . "%columns% FROM %table% WHERE %conditions%";
        
        if (!empty($columns) && '*' != $columns) {
            $this->columns = $columns;
        }
        
        $this->query = $query;
        return $this;
    }
    
    /**
     * Insert in the database
     * @param array $values
     * @param array $columns
     */
    public function insert($values, $columns = array())
    {
        $this->clear();
        
        if (empty($values)) {
            $this->query = '';
            return;
        }
        
        $this->columns = $columns;
        $query = 'INSERT INTO %table% '
            . (!empty($columns) ? '(%columns%) ' : '')
            . 'VALUES (';
        
        foreach ($values as $value) {
            if (is_string($value)) {
                $query .= "'$value', ";
            } elseif (is_int($value) || is_bool($value)) {
                $query .= "$value, ";
            }
        }
        
        $this->query = rtrim($query, ', ') . ')';
        return $this;
    }
    
    /**
     * Update
     * @param array $expressions
     */
    public function update($expressions)
    {
        $this->clear();
        
        if (empty($expressions)) {
            $this->query = '';
            return;
        }
        
        $query = 'UPDATE %table% SET ';
        
        foreach ($expressions as $key => $value) {
            if (is_string($value)) {
                $query .= "$key = '$value', ";
            } elseif (is_int($value) || is_bool($value)) {
                $query .= "$key = $value, ";
            }
        }
        
        $this->query = rtrim($query, ', ');
        return $this;
    }
    
    /**
     * Delete
     */
    public function delete()
    {
        $this->clear();
        $this->query = "DELETE FROM %table% WHERE %conditions%";
        return $this;
    }
    
    /**
     * Set table in the database which used in the query
     * @param string $table
     */
    public function from($table = 'books', $synonym = '')
    {
        $this->table = $table . (!empty($synonym) ? " $synonym" : '');
        return $this;
    }
    
    /**
     * Add condition
     * @param string $condition
     */
    public function where($condition)
    {
        $this->conditions[] = $condition;
        return $this;
    }
    
    /**
     * Set limit of response rows
     * @param int $limit
     * @param int $start
     */
    public function limit(int $limit, int $start = 0)
    {
        $this->limit = $limit;
        $this->start = $start;
        return $this;
    }
    
    /**
     * Set order for response rows in string format or in array: "column" => "ASC|DESC"
     * @param string|array $order
     */
    public function order($order)
    {
        if (is_array($order)) {
            $sql = '';
            
            foreach ($order as $column => $sort) {
                $sql .= "$column $sort, ";
            }
            
            $this->order = rtrim($sql, ', ');
        } else {
            $this->order = $order;
        }
        
        return $this;
    }
    
    /**
     * Set group by select command
     * @param string|array $order
     * @param array $having optional
     */
    public function group($group, $having = array())
    {
        $this->group = (is_array($group)) ? implode(", ", $group) : $group;

        if (!empty($having)) {
            $this->group .= " HAVING " . ((is_array($having)) ? implode(" AND ", $having) : $having);
        }
        
        return $this;
    }
    
    /**
     * Return query string
     * @return string|boolean
     */
    public function query()
    {
        if (empty($this->query) || empty($this->table)) {
            return false;
        }
    
        $query = str_replace('%table%', $this->table, $this->query);
        
        if (!empty($this->columns)) {
            $columns = (is_array($this->columns)) ? implode(", ", $this->columns) : $this->columns;
            $query = str_replace('%columns%', $columns, $query);
        } elseif (strpos($query, '%columns%')) {
            return false;
        }
        
        if (empty($this->conditions)) {
            $query = str_replace(' WHERE %conditions%', '', $query);
        } else {
            $query = str_replace('%conditions%', implode(' AND ', $this->conditions), $query);
        }
        
        # for select
        if (!empty($this->order)) {
            $query .= " ORDER BY " . $this->order;
        }
        
        if (!empty($this->group)) {
            $query .= " GROUP BY " . $this->group;
        }
        
        # limit
        if ($this->limit) {
            $query .= " LIMIT " . $this->limit;
            
            if ($this->start) {
                $query .= " OFFSET " . $this->start;
            }
        }
        
        return "$query;";
    }
    
    /**
     * Clear properties
     */
    private function clear()
    {
        $this->query = '';
        $this->columns = '*';
        $this->conditions = array();
        $this->limit = 0;
        $this->start = 0;
        $this->order = '';
        $this->group = '';
    }
}
