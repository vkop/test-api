<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

require_once '../vendor/autoload.php';

$request = \TestApi\Models\Request::getInstance();
$config = \TestApi\Models\Config::getInstance('../config.ini');
$db_config = $config->getDB();
$db = new \PDO(
    "pgsql:host={$db_config->host};"
    . "port={$db_config->port};"
    . "dbname={$db_config->name};"
    . "user={$db_config->user};"
    . "password={$db_config->password}"
);

$controllerClass = "\\TestApi\\Controllers\\"
    . ucfirst($request->getController()) . 'Controller';

if (!class_exists($controllerClass)) {
    $controllerClass = "\\TestApi\\Controllers\\DefaultController";
}

$entity = new $controllerClass($db, $request);
header('Content-type: application/json');
print $entity->response();
